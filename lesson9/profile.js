const express = require('express');
const router = express.Router();
const multer = require('multer');

const users = require('./users');

const upload = multer({ dest: 'static/pictures' });

router.get('/', (req, res) => {
  const isSigned = req.signedCookies.isSigned ? true : false;

  isSigned
    ? res.render('profile.ejs', { name: isSigned ? users.get(req.signedCookies.isSigned) || {} : { firstName: '', lastName: '', picture: {} } })
    : res.redirect(301, '/login');
});
router.post('/', upload.single('picture'), (req, res) => {
  if (!req.signedCookies.isSigned) { res.redirect(301, '/login'); }

  const user = users.get(req.signedCookies.isSigned) || {};

  const firstName = req.body.firstName || user.firstName || '';
  const lastName = req.body.lastName || user.lastName || '';
  const picture = req.file || user.picture || {};

  users.set(req.signedCookies.isSigned, {
    firstName,
    lastName,
    picture,
  });

  res.status(201).render('profile.ejs', {
    name: { firstName, lastName, picture },
  });
});

module.exports = router;
