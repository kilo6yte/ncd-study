const express = require('express');
const router = express.Router();
const multer = require('multer');

const upload = multer({ dest: 'static/pictures' });

router.get('/', (req, res) => res.render('login.ejs', { error: false }));
router.post('/', upload.single('picture'), (req, res) => {
  const name = req.body.name;
  const password = req.body.password;

  if (name === 'labs' && password === '42') {
    res
      .cookie('isSigned', name, { httpOnly: true, signed: true })
      .redirect(301, 'http://localhost:3000/profile');

    return;
  }

  res.status(401);
  res.render('login.ejs', { error: true, message: 'Incorrect login or password' });
});

module.exports = router;
