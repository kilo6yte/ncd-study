const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const cookieParser = require('cookie-parser');
const compression = require('compression');
const serveStatic = require('serve-static');

const loginRoutes = require('./login');
const profileRoutes = require('./profile');

const users = require('./users');

const app = express();
const port = 3000;


app.disable('x-powered-by');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser('123'));
app.use('/static', compression({ filter: () => true }));
app.use('/static', serveStatic(path.join(__dirname, 'static'), { maxAge: '3d' }));

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');


app.get('/', (req, res) => {
  const isSigned = req.signedCookies.isSigned ? true : false;

  res.render('index.ejs', {
    isSigned,
    name: isSigned ? users.get(req.signedCookies.isSigned) : {},
  });
});

app.use('/profile', profileRoutes);
app.use('/login', loginRoutes);

app.get('/logout', (req, res) => {
  res.clearCookie('isSigned');
  res.redirect(301, '/');
});


app.listen(port, () => console.log(`Example app listening on port ${port}!`));
