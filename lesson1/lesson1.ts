import * as zlib from 'zlib';
import * as util from 'util';

import { longString } from './longString';

const gzipCompress = util.promisify<string, Buffer>(zlib.gzip);
const brotliCompress = util.promisify<string, Buffer>(zlib.brotliCompress);
const deflateCompress = util.promisify<string, Buffer>(zlib.deflate);

function compress(
  text: string,
  type: 'gzip' | 'deflate' | 'brotli' = 'gzip',
  encoding: 'base64' | 'hex' = 'hex'): Promise<string> {

  switch (type) {
    case 'gzip':
      return gzipCompress(text).then(result => result.toString(encoding));

    case 'brotli':
      return brotliCompress(text).then(result => result.toString(encoding));

    case 'deflate':
      return deflateCompress(text).then(result => result.toString(encoding));
  }
}

const gzipDecompress = util.promisify<Buffer, Buffer>(zlib.gunzip);
const brotliDecompress = util.promisify<Buffer, Buffer>(zlib.brotliDecompress);
const inflateDecompress = util.promisify<Buffer, Buffer>(zlib.inflate);

function decompress(
  source: string,
  type: 'gzip' | 'deflate' | 'brotli' = 'gzip',
  encoding: 'base64' | 'hex' = 'hex'): Promise<string> {

  switch (type) {
    case 'gzip':
      return gzipDecompress(Buffer.from(source, encoding)).then(result => result.toString('utf8'));

    case 'brotli':
      return brotliDecompress(Buffer.from(source, encoding)).then(result => result.toString('utf8'));

    case 'deflate':
      return inflateDecompress(Buffer.from(source, encoding)).then(result => result.toString('utf8'));
  }
}


compress('gzip', 'gzip').then(res => {
  console.log('gzip - compressed.');
  decompress(res, 'gzip')
    .then(res => console.log('gzip - decompressed: ', res))
    .catch(e => console.log(e));
});
compress('brotli', 'brotli').then(res => {
  console.log('brotli - compressed.');
  decompress(res, 'brotli')
    .then(res => console.log('brotli - decompressed: ', res))
    .catch(e => console.log(e));
});
compress('deflate', 'deflate').then(res => {
  console.log('deflate - compressed.');
  decompress(res, 'deflate')
    .then(res => console.log('deflate - decompressed: ', res))
    .catch(e => console.log(e));
});


// TODO: Implement benchmark for compressing.
// async function benchmark(text: string): Promise<void> {

// }

// benchmark(longString);



/**
 * @example const formatter = new Formatter('ro-RO');
 * console.log(formatter.date(new Date())); // outputs: 23.09.2019
 * console.log(formatter.currency(42.5, 'lei')) // outputs: 42.50 lei
 * console.log(formatter.number(1024.42)) // outputs: 1,024.42
 */
class Formatter {
  private dateIntl: Intl.DateTimeFormat;

  private locale: string;

  constructor(locale: string) {
    this.locale = locale;
    this.dateIntl = new Intl.DateTimeFormat(locale);
  }

  public date(date: Date) {
    return this.dateIntl.format(date);
  }

  public currency(value: number, currency: string) {
    return new Intl.NumberFormat(this.locale, { style: 'currency', currency }).format(value);
  }

  public number(value: number) {
    return new Intl.NumberFormat(undefined, { useGrouping: true }).format(value);
  }

}

console.log('------FORMATER-------');
const formatter = new Formatter('de-DE');
console.log(formatter.date(new Date())); // outputs: 23.09.2019
console.log(formatter.currency(42.5, 'lei')) // outputs: 42.50 lei
console.log(formatter.number(1024.42)) // outputs: 1,024.42
console.log('--------------\n\n');



/**
 * Word Count
 * @example count('Crème Brûlée does not have creme.', 'creme'); // outputs 2
 * @example count('café cafe Cafe CAFE', 'cafe'); // outputs 4
 */
function count(text: string, searchedWord: string): number {

  const comapreFn = new Intl.Collator(undefined, { sensitivity: 'base', ignorePunctuation: true }).compare;

  return text.split(' ')
    .filter(word => comapreFn(word, searchedWord) === 0)
    .length;
}

console.log('------WORD COUNT-------');
console.log(count('Crème Brûlée does not have creme.', 'creme'));
console.log(count('café cafe Cafe CAFE', 'cafe'));
console.log('--------------\n\n');




/**
 * Tracing
 */
class Tracer {
  public enabled: boolean;

  constructor(mode: string = 'monitoring') {
    this.enabled = process.env.NODE_DEBUG === mode;
  }

  public trace(data: any, format?: any) {
    const formatData = format ? util.format(data, format) : data;
    console.log(util.inspect(formatData));
  }

}

const tracer = new Tracer('monitoring');

console.log('------TRACER-------');
console.log(tracer.enabled);
tracer.trace('In monitoring mode');
tracer.trace({ message: 'Hello world' });
tracer.trace('Message with data: %s', { x: 4, y: 2 });
console.log('--------------\n\n');
