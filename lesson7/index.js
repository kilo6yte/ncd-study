const http = require('http');
const url = require('url');

const api = require('./api');

const host = process.env.HOST || '0.0.0.0';
const port = process.env.PORT || 8080;

const server = http.createServer((req, res) => {
  const pathName = url.parse(req.url).pathname;

  if (pathName === '/' && req.method === 'GET') { return api.index.get(res, res); };

  if (pathName === '/calculate' && req.method === 'GET') { return api.calculate.get(req, res); }

  if (pathName === '/data' && req.method === 'POST') { return api.data.post(req, res); }

  if (pathName === '/visit' && req.method === 'GET') { return api.visit.get(req, res); }

  api.error(res);
});

server.listen({ port, host }, () => console.log(`Server is running on - ${host}:${port}`));
