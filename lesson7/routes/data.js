
const qs = require('querystring');

const dataRoutes = {
  dataPost: (req, res) => {
    let body = '';

    req.on('data', data => {
      body += data;
    });

    req.on('end', function () {
      const { username, password } = qs.parse(body);

      const isValidUser = username === 'labs42' && password === '1234';

      res.statusCode = isValidUser ? 200 : 401;
      res.end();
    });
  },
}

module.exports = dataRoutes;
