const indexRoutes = {
  indexGet: (req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');

    const data = JSON.stringify({ timestamp: new Date().toISOString() });

    res.end(data);
  }
}

module.exports = indexRoutes;
