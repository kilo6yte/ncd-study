const parseCookies = (req) => {
  const cookies = req.headers['cookie'];

  return cookies && cookies
    .split(';')
    .reduce((prev, cur) => {
      const cookiePair = cur.split('=');
      const key = cookiePair[0].trim();
      const value = cookiePair[1].trim();

      return { ...prev, [key]: value };
    }, {});
};

const visitRoutes = {
  visitGet: (req, res) => {
    res.statusCode = 200;
    const { count = 0 } = parseCookies(req) || {};
    const counter = parseInt(count, 10);

    res.writeHead(200, {
      'Content-Type': 'application/json',
      'Set-Cookie': [`count=${counter + 1}; HttpOnly`],
    });

    const data = JSON.stringify({ total: counter + 1 });
    res.end(data);
  }
}

module.exports = visitRoutes;
