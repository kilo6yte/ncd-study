const url = require('url');
const qs = require('querystring');
const http = require('http');

const calculateRoutes = {
  calculateGet: (req, res) => {
    const { a, b } = qs.parse(url.parse(req.url).query);

    res.setHeader('Content-Type', 'text/plain');

    if (!a || !b || !isNumber(a) || !isNumber(b)) {
      res.statusCode = 400;
      return res.end(`{"error": "${http.STATUS_CODES[400]}"}`);
    }

    res.statusCode = 200;
    const sum = parseInt(a, 10) + parseInt(b, 10);

    return res.end(sum.toString());
  },
}

module.exports = calculateRoutes;

const isNumber = (number) => number == parseInt(number, 10);
