const indexRoutes = require('./routes/index');
const calculateRoutes = require('./routes/calculate');
const dataRoutes = require('./routes/data');
const visitRoutes = require('./routes/visit');

const api = {
  index: {
    get: indexRoutes.indexGet,
  },
  calculate: {
    get: calculateRoutes.calculateGet,
  },
  data: {
    post: dataRoutes.dataPost,
  },
  visit: {
    get: visitRoutes.visitGet,
  },
  error: (res) => {
    res.statusCode = 404;

    res.end();
  },
}

module.exports = api;
