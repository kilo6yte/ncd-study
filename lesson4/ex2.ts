
import { Transform } from 'stream';

export default class Hex extends Transform {
  constructor(options) {
    super(options);
  }

  _transform(chunk: Buffer, encoding, callback) {
    callback(null, chunk.toString('hex'));
  }
}

// process.stdin.pipe(new Hex({})).pipe(process.stdout);

// echo "hello" | node index.js
// # prints: 68656c6c6f0d0a  //68656c6c6f 68656c6c6f
