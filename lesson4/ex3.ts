import * as crypto from 'crypto';
import { Readable } from 'stream';

export default class RandomReader extends Readable {
  private totalSize: number;
  private actualSize = 0;

  constructor(size) {
    super();
    this.totalSize = size;
  }

  _read(readSize) {
    let shouldEnd = false;

    if ((this.actualSize + readSize) >= this.totalSize) {
      readSize = this.totalSize - this.actualSize;
      shouldEnd = true;
    }

    crypto.randomBytes(readSize, (error, buffer) => {
      if (error) {
        this.emit('error', error);
        return;
      }

      this.actualSize += readSize;
      this.push(buffer);

      if (shouldEnd) {
        this.push(null);
      }
    });
  }
}

// const reader = new RandomReader(10) // 1MB of random data
// reader.pipe(process.stdout);