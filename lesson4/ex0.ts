
const int8 = Int8Array.from([13, 21, 3]);
console.log(int8);
console.log(int8.byteLength);
console.log(int8 instanceof Int8Array);

const int16 = Int16Array.from(int8);
console.log(int16);
console.log(int16.byteLength);
console.log(int16 instanceof Int16Array);

const int8new = Int8Array.from(int16);
console.log(int8new);
console.log(int8new.byteLength);
console.log(int8new instanceof Int8Array);
