import * as fs from 'fs';
import { Transform, pipeline } from 'stream';

import RandomReader from './ex3';
import Hex from './ex2';

class Limiter extends Transform {
  /** @rate represent the max number of bytes per second */
  private rate: number = 0;
  private sentBytes = 0;

  constructor(rate: number, options = {}) {
    super(options);

    this.rate = rate;
  }

  _transform(chunk: Buffer, encoding, callback) {
    const handle = () => {
      const data = Buffer.alloc(this.rate);
      chunk.copy(data, 0, this.sentBytes, this.sentBytes + this.rate);

      this.sentBytes += this.rate;

      if (data) {
        this.push(data);
      }

      if (this.sentBytes >= chunk.byteLength) {
        console.log('\n', chunk);
        callback(null);
        clearInterval(interval);
      }
    }

    const interval = setInterval(handle, 1000);
  }
}

new RandomReader(10) // 10KB of random data
  .pipe(new Limiter(1)) // 1KB per second throughput
  .pipe(new Hex({})) // print binary data in `hex` format
  .pipe(process.stdout);
// Finishes print and execution in 10 seconds



// echo Hello | node ex4
// Should finish writin "Hello" in 6 seconds.

// process.stdin.pipe(new Limiter(1)).pipe(process.stdout);