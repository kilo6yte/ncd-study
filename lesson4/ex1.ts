import { Transform } from 'stream';

class UpperCase extends Transform {
  constructor(options) {
    super(options);
  }

  _transform(chunk, encoding, callback) {
    callback(null, chunk.toString().toUpperCase());
  }
}

process.stdin.pipe(new UpperCase({})).pipe(process.stdout);

// call the script:
// echo hello | node index.js
// prints: HELLO
