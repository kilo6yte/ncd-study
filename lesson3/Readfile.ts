import * as fs from 'fs';
import * as readline from 'readline';
import { once } from 'events';

async function readFile(fileName: string) {
  const fileStream = fs.createReadStream('./lesson3/' + fileName);

  const rl = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity
  });

  console.log('File begin\n----------------\n');

  rl.on('line', (line) => {
    console.log(line);
  });

  await once(rl, 'close');

  console.log('----------------\nFile end.');
}

readFile('ex3.txt');