import * as readline from 'readline';
import * as os from 'os';


const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: 'L42: > ',
});


rl.write(`Hello Labs42\nPlease select a number for command:
1. process
2. os
3. time
4. exit\n`);

rl.prompt();
rl.on('line', (line) => {
  switch (line.trim()) {
    case '1':
      console.log(`Process title: ${process.title} / Process PID: ${process.pid}`);
      break;
    case '2':
      console.log(`OS type: ${os.type} / OS Arch: ${os.arch}`);
      break;
    case '3':
      console.log(`Current time: ${new Date()}`);
      break;
    case '4':
      console.log('Bye');
      process.exit(0);
    default:
      console.log('Unknown command. Select number from list.');
      break;
  }
  rl.prompt();
}).on('close', () => {
  console.log('Bye');
  process.exit(0);
});
