const isPrime = (x: number): boolean => {
  if (!Number.isInteger(x) || x < 0) { throw new Error('Invalid input/number.') };

  if (x < 2) { return false; }
  if (x % 2 == 0) { return (x == 2); }
  if (x % 3 == 0) { return (x == 3); }

  for (var i = 5; i <= Math.sqrt(x); i = i + 6) {
    if ((x % i == 0) || (x % (i + 2) == 0)) { return false; }
  }

  return true;
}

console.log(isPrime(7));
console.log(isPrime(8));
console.log(isPrime(13));
console.log(isPrime(111));
